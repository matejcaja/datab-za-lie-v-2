-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.31-0ubuntu0.12.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-05-06 01:08:47
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for DBS2013
DROP DATABASE IF EXISTS `DBS2013`;
CREATE DATABASE IF NOT EXISTS `DBS2013` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `DBS2013`;


-- Dumping structure for table DBS2013.ActualPrice
DROP TABLE IF EXISTS `ActualPrice`;
CREATE TABLE IF NOT EXISTS `ActualPrice` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `from` varchar(50) DEFAULT NULL,
  `to` varchar(60) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `drug_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ActualPrice_ibfk_1` (`drug_id`),
  CONSTRAINT `ActualPrice_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `Drugs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.ActualPrice: ~55 rows (approximately)
DELETE FROM `ActualPrice`;
/*!40000 ALTER TABLE `ActualPrice` DISABLE KEYS */;
INSERT INTO `ActualPrice` (`id`, `from`, `to`, `price`, `drug_id`) VALUES
	(1, '2013-04-08', '2013-04-14', 5.99, 1),
	(2, '2010-03-03', '2013-04-13', 2.59, 2),
	(3, '2012-03-03', '2013-05-05', 3.2, 3),
	(4, '2012-12-12', '2013-05-06', 4.5, 4),
	(5, '20010-05-06', NULL, 4.2, 5),
	(6, '20011-07-09', NULL, 0.99, 6),
	(7, '2012-09-21', '2013-03-02', 1.6, 7),
	(8, '2013-01-01', '2013-05-04', 4, 8),
	(9, '2012-03-10', '2013-04-05', 3.9, 9),
	(11, '2011-06-18', '2013-04-09', 2.5, 11),
	(12, '2012-02-23', NULL, 5.19, 12),
	(13, '2011-08-03', '2013-04-10', 5.3, 13),
	(14, '2012-03-25', NULL, 3.33, 14),
	(15, '2013-01-12', NULL, 3.99, 15),
	(16, '2012-06-06', NULL, 3.2, 16),
	(17, '2013-03-18', '2013-05-05', 11.2, 17),
	(19, '2012-09-17', NULL, 1.35, 19),
	(20, '2012-10-10', '2013-04-13', 25.2, 20),
	(26, '2013-04-13', '2013-04-12', 100, 1),
	(27, '2013-04-13', '2013-04-13', 20, 2),
	(32, '2013-04-13', '2013-04-13', 2.99, 1),
	(33, '2013-04-13', '2013-04-14', 2.5, 2),
	(34, '2013-04-13', '2013-04-13', 3.99, 20),
	(35, '2013-04-13', NULL, 5.99, 20),
	(37, '2013-04-13', NULL, 1.99, 11),
	(38, '2013-04-13', '2013-04-13', 1.2, 9),
	(39, '2013-04-13', NULL, 3.2, 7),
	(40, '2013-04-13', '2013-02-02', 2.99, 9),
	(42, '2013-04-13', NULL, 1.99, 13),
	(43, '2013-04-14', '2013-04-14', 3.99, 1),
	(45, '2013-04-14', NULL, 0.99, 1),
	(46, '2013-04-14', NULL, 3.45, 2),
	(82, '2013-05-05', NULL, 200, 3),
	(83, '2013-05-05', NULL, 100, 9),
	(84, '\'2013-05-05\'', NULL, 200, 8),
	(87, '2013-05-05', NULL, 1200, 17),
	(94, '2013-05-06', NULL, 1000, 83),
	(95, '2013-05-06', NULL, 100, 4),
	(100, '2013-05-06', '2013-05-06', 1000, 87),
	(101, '2013-05-06', NULL, 12, 87);
/*!40000 ALTER TABLE `ActualPrice` ENABLE KEYS */;


-- Dumping structure for table DBS2013.ChemicalRegistration
DROP TABLE IF EXISTS `ChemicalRegistration`;
CREATE TABLE IF NOT EXISTS `ChemicalRegistration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `valid_until` date DEFAULT NULL,
  `chemical_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ChemicalRegistration_ibfk_1` (`chemical_id`),
  CONSTRAINT `ChemicalRegistration_ibfk_1` FOREIGN KEY (`chemical_id`) REFERENCES `EffectiveChemical` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.ChemicalRegistration: ~12 rows (approximately)
DELETE FROM `ChemicalRegistration`;
/*!40000 ALTER TABLE `ChemicalRegistration` DISABLE KEYS */;
INSERT INTO `ChemicalRegistration` (`id`, `date`, `valid_until`, `chemical_id`) VALUES
	(1, '2008-08-03', '2013-12-12', 1),
	(2, '2007-07-09', '2014-04-11', 2),
	(3, '1991-02-18', '2015-12-25', 3),
	(4, '1999-09-05', '2056-09-21', 4),
	(5, '2003-04-06', '2013-10-25', 5),
	(6, '2006-05-03', '2013-01-01', 6),
	(7, '2001-12-14', '2012-12-31', 7),
	(8, '2005-04-01', '2013-02-02', 8),
	(9, '2013-04-09', '2016-08-14', 9),
	(10, '2009-03-18', '2015-04-13', 10),
	(11, '1880-02-04', '2013-04-01', 11),
	(12, '1996-04-03', '2012-07-12', 12);
/*!40000 ALTER TABLE `ChemicalRegistration` ENABLE KEYS */;


-- Dumping structure for table DBS2013.Contents
DROP TABLE IF EXISTS `Contents`;
CREATE TABLE IF NOT EXISTS `Contents` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `drug_id` int(10) NOT NULL,
  `chemical_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Contents_ibfk_1` (`chemical_id`),
  KEY `Contents_ibfk_2` (`drug_id`),
  CONSTRAINT `Contents_ibfk_1` FOREIGN KEY (`chemical_id`) REFERENCES `EffectiveChemical` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Contents_ibfk_2` FOREIGN KEY (`drug_id`) REFERENCES `Drugs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.Contents: ~31 rows (approximately)
DELETE FROM `Contents`;
/*!40000 ALTER TABLE `Contents` DISABLE KEYS */;
INSERT INTO `Contents` (`id`, `drug_id`, `chemical_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 8),
	(4, 4, 4),
	(5, 4, 5),
	(6, 5, 4),
	(7, 5, 5),
	(8, 6, 6),
	(9, 7, 7),
	(10, 8, 2),
	(11, 9, 2),
	(12, 9, 3),
	(14, 11, 1),
	(15, 12, 4),
	(16, 13, 4),
	(17, 14, 8),
	(18, 15, 8),
	(19, 15, 10),
	(20, 16, 8),
	(21, 17, 9),
	(23, 19, 12),
	(24, 20, 11),
	(29, 83, 1),
	(33, 87, 9);
/*!40000 ALTER TABLE `Contents` ENABLE KEYS */;


-- Dumping structure for table DBS2013.Doctor
DROP TABLE IF EXISTS `Doctor`;
CREATE TABLE IF NOT EXISTS `Doctor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `salary` float NOT NULL,
  `contact` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.Doctor: ~5 rows (approximately)
DELETE FROM `Doctor`;
/*!40000 ALTER TABLE `Doctor` DISABLE KEYS */;
INSERT INTO `Doctor` (`id`, `name`, `salary`, `contact`) VALUES
	(1, 'Jan Zaskocil', 1500, 954236654),
	(2, 'Frantisek Orezal', 1400, 959468786),
	(3, 'Bozidara Bezdarova', 2000, 995445522),
	(4, 'Silvester Strasak', 1459.99, 955245786),
	(5, 'Michal Trolovy', 389.5, 959111000);
/*!40000 ALTER TABLE `Doctor` ENABLE KEYS */;


-- Dumping structure for table DBS2013.Drugs
DROP TABLE IF EXISTS `Drugs`;
CREATE TABLE IF NOT EXISTS `Drugs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `holder` varchar(60) NOT NULL,
  `stav` char(12) NOT NULL,
  `vydaj` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.Drugs: ~29 rows (approximately)
DELETE FROM `Drugs`;
/*!40000 ALTER TABLE `Drugs` DISABLE KEYS */;
INSERT INTO `Drugs` (`id`, `name`, `holder`, `stav`, `vydaj`) VALUES
	(1, 'Paralen', 'DrugStore', 'able', 'on prescript'),
	(2, 'Paralen', 'PharmaGuy', 'able', 'on prescript'),
	(3, 'Strepsils', 'PharmaGuy', 'able', 'free'),
	(4, 'Augmentim', 'PharmaGuy', 'not able', 'on prescript'),
	(5, 'Augmentim', 'DrugStore', 'able', 'free'),
	(6, 'Kalcid', 'DrugStore', 'able', 'free'),
	(7, 'ACC Short', 'PharmaGuy', 'able', 'on prescript'),
	(8, 'Strepsos', 'DrugStore', 'able', 'free'),
	(9, 'Strepsils', 'DrugStore', 'able', 'on prescript'),
	(11, 'Paralon', 'PharmaGuy', 'able', 'on prescript'),
	(12, 'Augmentom', 'DrugStore', 'not able', 'free'),
	(13, 'Augmentom', 'PharmaGuy', 'able', 'free'),
	(14, 'Espumison', 'PharmaGuy', 'able', 'free'),
	(15, 'Espumisan', 'PharmaGuy', 'able', 'free'),
	(16, 'Espumison', 'DrugStore', 'not able', 'free'),
	(17, 'Warfaron', 'DrugStore', 'not able', 'on prescript'),
	(19, 'Oilatum', 'DrugStore', 'able', 'on prescript'),
	(20, 'Canabis', 'PharmaGuy', 'able', 'on prescript'),
	(83, 'Liek', 'DrugStore', 'able', 'on prescript'),
	(87, 'kill', 'DrugStore', 'not able', 'on prescript');
/*!40000 ALTER TABLE `Drugs` ENABLE KEYS */;


-- Dumping structure for table DBS2013.DrugsForm
DROP TABLE IF EXISTS `DrugsForm`;
CREATE TABLE IF NOT EXISTS `DrugsForm` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.DrugsForm: ~8 rows (approximately)
DELETE FROM `DrugsForm`;
/*!40000 ALTER TABLE `DrugsForm` DISABLE KEYS */;
INSERT INTO `DrugsForm` (`id`, `type`) VALUES
	(1, 'tabletky'),
	(2, 'sprej'),
	(3, 'gel'),
	(4, 'capiky'),
	(5, 'draze'),
	(6, 'kvapky'),
	(7, 'vytazky'),
	(8, 'pilulky');
/*!40000 ALTER TABLE `DrugsForm` ENABLE KEYS */;


-- Dumping structure for table DBS2013.DrugsSpecification
DROP TABLE IF EXISTS `DrugsSpecification`;
CREATE TABLE IF NOT EXISTS `DrugsSpecification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type_id` bigint(10) DEFAULT NULL,
  `drug_id` int(10) DEFAULT NULL,
  `sell_start` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `DrugsSpecification_ibfk_1` (`drug_id`),
  KEY `DrugsSpecification_ibfk_2` (`type_id`),
  CONSTRAINT `DrugsSpecification_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `Drugs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DrugsSpecification_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `DrugsForm` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.DrugsSpecification: ~30 rows (approximately)
DELETE FROM `DrugsSpecification`;
/*!40000 ALTER TABLE `DrugsSpecification` DISABLE KEYS */;
INSERT INTO `DrugsSpecification` (`id`, `type_id`, `drug_id`, `sell_start`) VALUES
	(1, 1, 1, '2012-02-02'),
	(2, 1, 2, '2012-03-03'),
	(3, 5, 3, '2012-04-03'),
	(4, 6, 4, '2010-12-22'),
	(5, 2, 5, '2009-01-15'),
	(6, 7, 6, '2013-01-19'),
	(7, 4, 7, '2011-04-30'),
	(8, 8, 8, '2010-12-12'),
	(9, 4, 9, '2012-07-05'),
	(11, 3, 11, '2009-03-06'),
	(12, 5, 12, '2012-12-12'),
	(13, 5, 13, '0000-00-00'),
	(14, 2, 14, '2009-10-15'),
	(15, 1, 15, '2006-05-14'),
	(16, 2, 16, '2003-09-11'),
	(17, 6, 17, '0000-00-00'),
	(19, 4, 19, '2012-07-12'),
	(20, 7, 20, '2012-08-13'),
	(21, 4, 1, '2012-05-15'),
	(22, 5, 9, '2010-02-15'),
	(59, 1, 83, '2013-05-06'),
	(63, 5, 87, '2013-05-06');
/*!40000 ALTER TABLE `DrugsSpecification` ENABLE KEYS */;


-- Dumping structure for table DBS2013.EffectiveChemical
DROP TABLE IF EXISTS `EffectiveChemical`;
CREATE TABLE IF NOT EXISTS `EffectiveChemical` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nameC` varchar(50) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.EffectiveChemical: ~12 rows (approximately)
DELETE FROM `EffectiveChemical`;
/*!40000 ALTER TABLE `EffectiveChemical` DISABLE KEYS */;
INSERT INTO `EffectiveChemical` (`id`, `nameC`, `state`) VALUES
	(1, 'paracetamil', 1),
	(2, 'amylmetakryzol', 1),
	(3, 'dichlorbenzinmetal', 1),
	(4, 'emoxicilin', 1),
	(5, 'kyselina klavylanova', 1),
	(6, 'kalciumon', 1),
	(7, 'acetylcystein', 1),
	(8, 'simetykon', 1),
	(9, 'warfaryn', 1),
	(10, 'parafon tuhy', 1),
	(11, 'canabin', 1),
	(12, 'benzydamon', 1);
/*!40000 ALTER TABLE `EffectiveChemical` ENABLE KEYS */;


-- Dumping structure for table DBS2013.Prescript
DROP TABLE IF EXISTS `Prescript`;
CREATE TABLE IF NOT EXISTS `Prescript` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `drug_id` int(10) NOT NULL,
  `doctor_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Prescript_ibfk_1` (`drug_id`),
  KEY `Prescript_ibfk_2` (`doctor_id`),
  CONSTRAINT `Prescript_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `Drugs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Prescript_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `Doctor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table DBS2013.Prescript: ~9 rows (approximately)
DELETE FROM `Prescript`;
/*!40000 ALTER TABLE `Prescript` DISABLE KEYS */;
INSERT INTO `Prescript` (`id`, `date`, `drug_id`, `doctor_id`) VALUES
	(1, '2009-02-15', 1, 1),
	(2, '2009-09-05', 2, 1),
	(3, '2012-09-21', 4, 2),
	(4, '2011-07-09', 9, 3),
	(5, '2010-08-15', 11, 3),
	(6, '2009-01-17', 17, 2),
	(7, '2013-01-28', 20, 1),
	(8, '2010-02-05', 19, 2),
	(9, '2009-07-10', 7, 3);
/*!40000 ALTER TABLE `Prescript` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
