/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package runners;

import gui.NewHibernateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import models.ActualPrice;
import models.Drugs;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author mato
 */
public class UpdateRunner {
    private DefaultListModel list;
    private DefaultListModel list2;
    private DefaultListModel list3;
    private JTextArea textArea;
    
    public UpdateRunner(){
        
    }
    
    public UpdateRunner(DefaultListModel list,DefaultListModel list2,DefaultListModel list3, JTextArea textArea){
        this.list = list;
        this.list2 = list2;
        this.list3 = list3;
        this.textArea = textArea;
    }
    
    
    public void executeFillinList(){
        executeHQLQueryList("select distinct d.name from Drugs d", list);
        executeHQLQueryList("select distinct d.holder from Drugs d", list2);
        executeHQLQueryList("select distinct d.stav from Drugs d", list3);
   
    }
    
     public void executeHQLQueryInsert(JList list4,JList list5,JList list6, JTextField textField){
         DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
         Date date = new Date();
         
         System.out.println(dateFormat.format(date));
         
         try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            
            Query q = session.createQuery("from Drugs d where "
                   + "d.name = '"+list4.getSelectedValue()+ "' and d.holder = '"+list5.getSelectedValue()+"'");
            List resultList = q.list();
            
            Drugs drug = null;
            for(Object o : resultList){
                drug = (Drugs)o;
            }
                       
            int a = session.createQuery("update Drugs set "
                    + "stav = '"+list6.getSelectedValue()+"' where id = "+drug.getId()
                    ).executeUpdate();
            
            
            int s = session.createQuery("update ActualPrice set price.to = '"+dateFormat.format(date)+"'"
                    + " where price.drugs.id = "+drug.getId()+" and price.to is null").executeUpdate();
            
            ActualPrice newPrice = new ActualPrice();
            newPrice.setDrugs(drug);
            newPrice.setPrice(Float.parseFloat(textField.getText()));
            newPrice.setFrom(dateFormat.format(date));
            newPrice.setTo(null);
            session.save(newPrice);
            
            textArea.append("Done!\nAktualizovalo sa "+a+" zaznamov!");
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
     }
    
    
    
    private void executeHQLQueryList(String hql, DefaultListModel list){
         try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            
            for(Object o : resultList){
                String drug = (String)o;
                list.addElement(drug);
            }
            
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
}
