/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package runners;

import gui.NewHibernateUtil;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import models.ActualPrice;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author mato
 */
public class SpecificRunner {
    private JTable jTable1;
    private JTextArea textArea;
    private JList list;
    private Object id;
    
    public SpecificRunner(){
        
    }
    
    public SpecificRunner(JTable jTable1, JTextArea textArea){
        this.jTable1 = jTable1;
        this.textArea = textArea;
    }
    
    public void getData(String choice, JList list){
        createHQLQuery(choice, list.getSelectedValue());
        createHQLQueryAll(id);
        createHQLQueryAvg(id);
        createHQLQueryMin(id);
    }
    
     public void createHQLQueryMin(Object id){
        try{
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        Query query = session.createQuery("select min(a.price)from ActualPrice a where a.drugs.id = "+id);
        List resultList = query.list();
        
        
        textArea.append("\nMin. cena lieku: ");
        
        for(Object o : resultList){
            Float price = (Float)o;
            textArea.append(price+"\n");
        }
        
        session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    public void createHQLQueryAvg(Object id){
        try{
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        Query query = session.createQuery("select avg(a.price)from ActualPrice a where a.drugs.id = "+id);
        List resultList = query.list();
        
        
        textArea.append("\nPriemerna cena lieku: ");
        
        for(Object o : resultList){
            Double price = (Double)o;
            textArea.append(price+"\n");
        }
        
        session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    public void createHQLQueryAll(Object id){
        
        try{
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        Query query = session.createQuery("from ActualPrice a where a.drugs.id = "+id);
        List resultList = query.list();
        
        textArea.append("Prehlad ceny lieku:\nod\tdo\tcena\n" +
	"-----------------------------------------------------------\n");
        
        for(Object o : resultList){
            ActualPrice price = (ActualPrice)o;
            textArea.append(price.getFrom()+"\t"+price.getTo()+"\t"+price.getPrice()+"\n");
        }
        
        session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    public void createHQLQuery(String holder, Object name) throws HibernateException{
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            
           Query query = session.createSQLQuery("select "
            + "*, e.nameC as wtf from Drugs d " +
            " join DrugsSpecification s on s.drug_id = d.id " +
            " join DrugsForm f on f.id = s.type_id " +
            " join ActualPrice a on a.drug_id = d.id " +
            " join Contents c on c.drug_id = d.id " +
            " join EffectiveChemical e on e.id = c.chemical_id " +
            " where d.name = '"+name+"' and d.holder = '"+ holder+"' and a.to is null");
            
           
            query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            List resultList = query.list();
            
            displayResult(resultList, jTable1);
            
            session.getTransaction().commit();
      
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    private void displayResult(List resultList, JTable jTable1) {

        Vector<String> tableHeaders = new Vector<String>();
        Vector tableData = new Vector();
        tableHeaders.add("id"); 
        tableHeaders.add("Name");
        tableHeaders.add("Holder");
        tableHeaders.add("State");
        tableHeaders.add("Vydaj");
        tableHeaders.add("Cena");
        tableHeaders.add("Latka");
        tableHeaders.add("Typ");
        
        Map row = null;
        
        for(Object object : resultList){
                row = (Map)object;
                Vector<Object> oneRow = new Vector<Object>();  
                oneRow.add(row.get("id"));
                oneRow.add(row.get("name"));
                oneRow.add(row.get("holder"));
                oneRow.add(row.get("stav"));
                oneRow.add(row.get("vydaj"));
                oneRow.add(row.get("price"));
                oneRow.add(row.get("nameC"));
                oneRow.add(row.get("type"));
              
                tableData.add(oneRow); 
        }
  
        jTable1.setModel(new DefaultTableModel(tableData, tableHeaders));
        
        id = row.get("id");
    
    }
    
}
