package runners;

import gui.NewHibernateUtil;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import javax.swing.JTable;
import javax.swing.JTextArea;
import models.Drugs;
import models.DrugsInfo;


public class DrugsRunner {
    private JTable jTable1;
    private String able = "'able'";
    private String price = "1000";
    private String order = "'asc'";
    private String holder;
    private DefaultListModel list;
    private DefaultListModel list2;
    private JTextArea jTextArea1;
  
    
    public DrugsRunner(JTable jTable1, DefaultListModel list, DefaultListModel list2, JTextArea jTextArea1){
        this.jTable1 = jTable1;
        this.list = list;
        this.list2 = list2;
      
        this.jTextArea1 = jTextArea1;
        
    }
    
    public void executeShowDrugs(){
       executeHQLQuery("select new models.DrugsInfo(price.drugs.id, price.drugs.name, "
               + "price.drugs.holder, price.drugs.stav, price.price) from "
               + "ActualPrice price where price.to is null ");
              
              
    }

    public void executeAdvancedShowDrugs(){
       executeHQLQuery("select new models.DrugsInfo(price.drugs.id, price.drugs.name, "
               + "price.drugs.holder, price.drugs.stav, price.price) from "
               + "ActualPrice price where price.to is null and price.drugs.stav = "+able
               + " and price.price < " +price
               + " and price.drugs.holder = " +holder
               + " order by price.drugs.id "+ order);
    }
    
    public void executeFillinList(){
        executeHQLQueryList("select distinct d.name from Drugs d", list);
        executeHQLQueryList("select distinct d.holder from Drugs d", list2);

    }
    
    
    public void executeShowDrugToDelete(JList list,JList list2){
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("from Drugs c where c.name = \'"+list.getSelectedValue()+"\' and c.holder = "
                + "\'"+list2.getSelectedValue()+"\'");
            List resultList = q.list();
            
            for(Object o : resultList){
                Drugs d = (Drugs)o;
                jTextArea1.append("Chystate sa zmazat:\n"+d.getName()+"\n"+d.getHolder() + "\n"+d.getStav()+"\n"+d.getVydaj());
            }
            
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    public void executeHQLQueryDeleteDrug(JList list,JList list2){
       
        String hql = "delete Drugs c where c.name = \'"+list.getSelectedValue()+"\' and c.holder = "
                + "\'"+list2.getSelectedValue()+"\'";
       
        
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            
            int a = session.createQuery(hql).executeUpdate();

            session.getTransaction().commit();
            jTextArea1.setText("Done!\nZmazanych "+a+" zaznamov!");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
   
    public void executeHQLQueryList(String hql, DefaultListModel list){
         try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            
            for(Object o : resultList){
                String drug = (String)o;
                list.addElement(drug);
            }
            
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    private void executeHQLQuery(String hql) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            displayResult(resultList, jTable1);
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    private void displayResult(List resultList, JTable jTable1) {

        Vector<String> tableHeaders = new Vector<String>();
        Vector tableData = new Vector();
        tableHeaders.add("id"); 
        tableHeaders.add("Name");
        tableHeaders.add("Holder");
        tableHeaders.add("State");
        tableHeaders.add("Cena");
        
        for(Object o : resultList){       
            Vector<Object> oneRow = new Vector<Object>();     
            DrugsInfo info = (DrugsInfo)o;  
            oneRow.add(info.getId());
            oneRow.add(info.getName());
            oneRow.add(info.getHolder());
            oneRow.add(info.getStav());
            oneRow.add(info.getPrice());
            tableData.add(oneRow);       
        }
            
        jTable1.setModel(new DefaultTableModel(tableData, tableHeaders));
        
    
    }
    
    public void setAble(String able){
	if(able.equals("Ano"))
            this.able = "'able'";
        else
            this.able = "'not able'";
		
    }
    
    public void setPrice(String price){
		this.price = price;
	}
    
    public void setOrder(String order){
		this.order = order;
	}
    
    public void setHolder(String data){
		String a = "'"+data+"'";
		this.holder = a;
	}
}
