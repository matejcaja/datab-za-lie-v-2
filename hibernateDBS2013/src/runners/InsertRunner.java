/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package runners;

import gui.NewHibernateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import models.ActualPrice;
import models.Contents;
import models.Drugs;
import models.DrugsForm;
import models.DrugsSpecification;
import models.EffectiveChemical;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author mato
 */
public class InsertRunner {
    private DefaultListModel list;
    private DefaultListModel list2;
    private DefaultListModel list3;
    private DefaultListModel list4;
    private DefaultListModel list5;
    private JTextField textField;
    private JTextField textField2;
    private JTextArea textArea;
    
    
    public InsertRunner(){
        
    }
    public InsertRunner(DefaultListModel list,DefaultListModel list2,DefaultListModel list3,
            DefaultListModel list4,DefaultListModel list5,JTextField textField, JTextField textField2, JTextArea textArea){
        this.list = list;
        this.list2 = list2;
        this.list3 = list3;
        this.list4 = list4;
        this.list5 = list5;
        this.textField = textField;
        this.textField2 = textField2;
        this.textArea = textArea;
    }
    
    public void executeFillinList(){
        executeHQLQueryList("select distinct d.holder from Drugs d", list);
        executeHQLQueryList("select distinct d.stav from Drugs d", list2);
        executeHQLQueryList("select distinct d.vydaj from Drugs d", list3);
        executeHQLQueryList("select d.type from DrugsForm d", list4);
        executeHQLQueryList("select d.nameC from EffectiveChemical d", list5);
        
    }
    
    public void executeHQLQueryInsert(JList list6,JList list7,JList list8,JList list9,JList list10) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        Long z = Long.parseLong((list9.getSelectedIndex()+1)+"");
        Long x = Long.parseLong((list10.getSelectedIndex()+1)+"");
        
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            
            Drugs drug = new Drugs();
            drug.setName(textField.getText());
            drug.setHolder((String) list6.getSelectedValue());
            drug.setStav((String) list7.getSelectedValue());
            drug.setVydaj((String) list8.getSelectedValue());
            session.save(drug);
            
            ActualPrice price = new ActualPrice();
            price.setDrugs(drug);
            price.setPrice(Float.parseFloat(textField2.getText()));
            price.setFrom(dateFormat.format(date));
            price.setTo(null);
            session.save(price);
            
            DrugsForm form = new DrugsForm();
            form.setType((String) list9.getSelectedValue());
            form.setId(z);
            session.saveOrUpdate(form);
            
            DrugsSpecification specify = new DrugsSpecification();
            specify.setDrugs(drug);
            specify.setDrugsForm(form);
            specify.setSellStart(dateFormat.format(date));
            session.save(specify);
            
            EffectiveChemical chem = new EffectiveChemical();
            chem.setNameC((String) list10.getSelectedValue());
            chem.setId(x);
            chem.setState(true);
            session.saveOrUpdate(chem);
            
            Contents con = new Contents();
            con.setDrugs(drug);
            con.setEffectiveChemical(chem);
            session.save(con);
            
            textArea.append("Done!\n");
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    
    private void executeHQLQueryList(String hql, DefaultListModel list){
         try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            
            for(Object o : resultList){
                String drug = (String)o;
                list.addElement(drug);
            }
            
            session.getTransaction().commit();
    
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
   
}
