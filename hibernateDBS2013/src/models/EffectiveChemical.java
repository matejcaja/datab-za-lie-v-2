package models;
// Generated May 4, 2013 2:28:14 PM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * EffectiveChemical generated by hbm2java
 */
public class EffectiveChemical  implements java.io.Serializable {


     private Long id;
     private String nameC;
     private boolean state;
     private Set chemicalRegistrations = new HashSet(0);
     private Set contentses = new HashSet(0);

    public EffectiveChemical() {
    }

	
    public EffectiveChemical(String nameC, boolean state) {
        this.nameC = nameC;
        this.state = state;
    }
    public EffectiveChemical(String nameC, boolean state, Set chemicalRegistrations, Set contentses) {
       this.nameC = nameC;
       this.state = state;
       this.chemicalRegistrations = chemicalRegistrations;
       this.contentses = contentses;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getNameC() {
        return this.nameC;
    }
    
    public void setNameC(String nameC) {
        this.nameC = nameC;
    }
    public boolean isState() {
        return this.state;
    }
    
    public void setState(boolean state) {
        this.state = state;
    }
    public Set getChemicalRegistrations() {
        return this.chemicalRegistrations;
    }
    
    public void setChemicalRegistrations(Set chemicalRegistrations) {
        this.chemicalRegistrations = chemicalRegistrations;
    }
    public Set getContentses() {
        return this.contentses;
    }
    
    public void setContentses(Set contentses) {
        this.contentses = contentses;
    }




}


