/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author mato
 */
public class DrugsInfo {
     private Integer id;
     private String name;
     private String holder;
     private String stav;
     private String vydaj;
     private float price;
     
     public DrugsInfo(){
         
     }
     
     public DrugsInfo(Integer id, String name, String holder, String stav, float price){
        this.id = id;
        this.name = name;
        this.holder = holder;
        this.stav = stav;
        this.price = price;
     }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the holder
     */
    public String getHolder() {
        return holder;
    }

    /**
     * @param holder the holder to set
     */
    public void setHolder(String holder) {
        this.holder = holder;
    }

    /**
     * @return the stav
     */
    public String getStav() {
        return stav;
    }

    /**
     * @param stav the stav to set
     */
    public void setStav(String stav) {
        this.stav = stav;
    }

    /**
     * @return the vydaj
     */
    public String getVydaj() {
        return vydaj;
    }

    /**
     * @param vydaj the vydaj to set
     */
    public void setVydaj(String vydaj) {
        this.vydaj = vydaj;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }
}
